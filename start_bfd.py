from os import popen

def start_bfd_sessions():
    def enable_bfd(iface):
        popen('sudo ovs-vsctl set interface %s bfd:enable=true' % iface, 'r')
        popen('sudo ovs-appctl bfd/set-forwarding %s true' % iface, 'r')

    def _interfaces():
        regex = "s[1-9]-eth[1-9]"
        return map(
                lambda line: line.strip(),
                popen('sudo ovs-vsctl list interface | grep -o %s' % regex).readlines()
                )

    [enable_bfd(iface) for iface in _interfaces()]

if __name__ == "__main__":
    start_bfd_sessions()
