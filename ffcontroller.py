import json
import logging
from os import popen

from webob import Response

from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ofproto_protocol
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib import bfdlib
from ryu.lib.packet import bfd
from ryu.controller import dpset
from ryu.app.wsgi import ControllerBase, WSGIApplication, route
from ryu.lib import dpid as dpid_lib
from ryu.lib import port_no as port_no_lib
from ryu.topology.api import get_switch, get_link
from ryu.topology import event, switches 
import networkx as nx

import config
from start_bfd import start_bfd_sessions

controller_instance_name = 'controller_api_app'
OUTPUT = 1
GROUP  = 2

class Controller(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    _CONTEXTS = {
        'dpset' : dpset.DPSet,
        'wsgi'  : WSGIApplication
    }

    def __init__(self, *args, **kwargs):
        super(Controller, self).__init__(*args, **kwargs)
        self.dpids = []
        self.datapaths = {}
        self.dpset  = kwargs['dpset']
        wsgi = kwargs['wsgi']
        wsgi.register(ControllerRestApi, {controller_instance_name : self})
        self.dpset  = kwargs['dpset']
        self.topology_api_app = self
        self.net=nx.DiGraph()
        self.links = []
        self.paths_calculated = False
        self.config = config.load()
        self.config_set = False
        self.tick = 1

    def _get_dp(self, dpid):
        return self.dpset.get(dpid)

    def _get_ports(self, dpid):
        if type(dpid) == str:
            return None

        return self.dpset.get_ports(dpid)

    def _get_ports_no(self, dpid):
        if type(dpid) == str:
            return None

        return [port.port_no for port in self._get_ports(dpid) if port.port_no < 100]

    def _has_link(self, src, dst):
        for link in self.links:
            if link[0] == src and link[1] == dst:
                return True
        return False

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        self.datapaths[datapath.id] = datapath
        self.dpids.append(datapath.id)
        self.logger.info("switch connected, dp: %s" % datapath.id)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)

    def setup_flows(self, dp, in_port, out_port, dst=None):
        ofp = dp.ofproto
        parser = dp.ofproto_parser
        actions = [parser.OFPActionOutput(out_port)]

        match = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_ARP)
        self.send_flow_msg(parser, dp, match, actions)

        if not dst:
            match2 = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_IP)
        else:
            match2 = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_IP, eth_dst=dst)

        self.send_flow_msg(parser, dp, match2, actions)

    def send_flow_msg(self, parser, dp, match, actions):
        ofproto = dp.ofproto
        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        mod = parser.OFPFlowMod(datapath=dp, match=match,
                cookie=0, command=ofproto.OFPFC_ADD, idle_timeout=0, hard_timeout=0,
                priority=9001, flags=ofproto.OFPFF_SEND_FLOW_REM,
                instructions=inst)

        dp.send_msg(mod)

    @set_ev_cls(event.EventLinkAdd)
    def link_add(self, ev):
        self.logger.info("[linkadd] %s, %s" % (ev.link.src, ev.link.dst))

    @set_ev_cls(event.EventSwitchEnter)
    def get_topology_data(self, ev):
        self.logger.info("[SwitchEnter]")
        switch_list = get_switch(self.topology_api_app, None)
        switches = [switch.dp.id for switch in switch_list]
        self.net.add_nodes_from(switches)

        links_list = get_link(self.topology_api_app, None)

        links = [(link.src.dpid, link.dst.dpid, {'port': link.src.port_no}) for link in links_list]
        self.net.add_edges_from(links)

        links = [(link.dst.dpid, link.src.dpid, {'port': link.dst.port_no}) for link in links_list]
        self.net.add_edges_from(links)

        [self.links.append(link) for link in links if not self._has_link(link[0], link[1])]

    def load_config(self):
        for sensor in self.config['sensors']:
            self.net.add_node(sensor)

        for icon in self.config['icontrollers']:
            self.net.add_node(icon)

        for link in self.config['links']:
            self.net.add_edge(int(link['dp']), link['mac'], {"port": int(link['port'])})
            self.net.add_edge(link['mac'], int(link['dp']))
            link_tuple = (int(link['dp']), link['mac'], {"port": int(link['port'])})

            if not self._has_link(link_tuple[0], link_tuple[1]):
                self.links.append(link_tuple)

    def calculate_paths(self):
        for src in self.config['sensors'] + self.config['icontrollers']:
            for dst in [n for n in self.config['sensors'] + self.config['icontrollers'] if n != src]:
                self.calculate_path(src, dst)
                

    def calculate_path(self, src, dst):

        def _actions(parser, port):
            return [parser.OFPActionOutput(port)]

        def send_group_mod(dp, ports):
            ofp = dp.ofproto
            ofp_parser = dp.ofproto_parser
            watch_group = ofp.OFPG_ANY

            buckets = [ofp_parser.OFPBucket(0, p, watch_group, _actions(ofp_parser, p)) for p in ports]

            req = ofp_parser.OFPGroupMod(dp, ofp.OFPGC_ADD, ofp.OFPGT_FF, self.tick, buckets)
            dp.send_msg(req)

            self.tick += 1
            return self.tick - 1

        def setup_flows_ff(dp, dst, group_id, in_port=None):
            ofp = dp.ofproto
            parser = dp.ofproto_parser
            actions   = [parser.OFPActionGroup(group_id)]

            if not in_port:
                match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_ARP, eth_dst=dst)
                self.send_flow_msg(parser, dp, match, actions)

                match2 = parser.OFPMatch(eth_dst=dst, eth_type=ether_types.ETH_TYPE_IP)
                self.send_flow_msg(parser, dp, match2, actions)

            else:
                match = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_ARP)
                self.send_flow_msg(parser, dp, match, actions)

                match2 = parser.OFPMatch(in_port=in_port, eth_dst=dst, eth_type=ether_types.ETH_TYPE_IP)
                self.send_flow_msg(parser, dp, match2, actions)

        def setup_arp_flows(dp, in_port, actions):
            parser = dp.ofproto_parser
            match = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_ARP)
            self.send_flow_msg(parser, dp, match, actions)

        def _port(src, dst, mode=None):
            for link in self.links:
                if link[0] == src and link[1] == dst:
                    return link[2]['port']

        def _host_ports(dpid):
            _hosts = [mac for mac in self.config['sensors'] + self.config['icontrollers']]
            _ports = []

            for link in self.links:
                if (link[0] in _hosts or link[1] in _hosts) and (
                        link[0] == dpid or link[1] == dpid):
                    _ports.append(link[2]['port'])

            return _ports

        def _nonhost_ports(dpid):
            return filter(lambda x: x not in _host_ports(dpid), self._get_ports_no(dpid))
           
        def _is_path_clean(src, path, host):
            for x in path:
                if type(x) == str and x not in [src, host]:
                    return False
            return True

        def _clean_paths(src, paths, host):
            return [path for path in paths if _is_path_clean(src, path, host)]

        
        if not self.config_set:
            self.load_config() 
                
            self.config_set = True

        def _next_port(src, dst, path):
            return _port(src, path[path.index(src) + 1])

        def _prev_port(src, path):
            return _port(src, path[path.index(src) - 1])
 
        def _is_host_port(src, port):
            return port in _host_ports(src)

        def _insert_arp_flows(src, data):
            dp = self._get_dp(src)
            parser = dp.ofproto_parser
            actions = []
            for in_port, tuples in data.items():
                for tup in tuples:
                    if tup[0] == GROUP:
                        action = parser.OFPActionGroup(tup[1])
                    elif tup[0] == OUTPUT:
                        action = parser.OFPActionOutput(tup[1])

                    actions.append(action)

            setup_arp_flows(dp, in_port, actions)
                

        def _insert_flows(src, tuples):
            host_ports_done = []
            _ports_done     = []
            _tuples_done = []
            _arp = {}

            for tup in tuples:
                in_port  = tup[0]
                out_port = tup[1]
                dst      = tup[2]
                dp       = self._get_dp(src)
                
                _arp.setdefault(in_port, [])

                if _is_host_port(src, in_port):
                    if _is_host_port(src, out_port):
                        setup_flows(self, dp, in_port, out_port, dst=dst)
                        _arp[in_port].append((OUTPUT, out_port, dst))
                        continue

                    if in_port in host_ports_done:
                        continue

                    ports = [t[1] for t in tuples if t[0] == in_port and t[2] == dst]
                    gid   = send_group_mod(dp, ports)
                    setup_flows_ff(dp, dst, gid, in_port=in_port)

                    host_ports_done.append(in_port)
                    _arp[in_port].append((GROUP, gid, dst))

                else:
                    self.setup_flows(dp, in_port, out_port, dst=dst)
                    _arp[in_port].append((OUTPUT, out_port, dst))
                    

            if len(_host_ports(src)) > 1:
                _insert_arp_flows(src, _arp)

        data = { }

        sp = nx.shortest_path(self.net, src, dst)
        alt_paths = nx.all_simple_paths(self.net, source=src, target=dst)
        alt_paths = sorted(filter(lambda x: x != sp, _clean_paths(src, alt_paths, dst)), key = len)

        paths = [sp]
        [paths.append(p) for p in alt_paths]

        for path in paths:
            for n in path:
                if type(n) != str:
                    data.setdefault(n, [])
                    out_port = _next_port(n, dst, path)
                    in_port  = _prev_port(n, path)
                    data[n].append((in_port, out_port, dst))

        for k, v in data.items():
            _insert_flows(k, v)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)

        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        dst = eth.dst
        src = eth.src

        dpid = datapath.id

        if src not in self.net:
            self.net.add_node(src)
            self.net.add_edge(dpid, src, {'port': in_port})
            self.net.add_edge(src, dpid)
        #self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)


url = '/switch'

class ControllerRestApi(ControllerBase):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
    def __init__(self, req, link, data, **config):
        super(ControllerRestApi, self).__init__(req, link, data, **config)
        self.controller_rest_app = data[controller_instance_name]

    @route('switch', url, methods=['GET']) 
    def get_datapaths(self, req, **kwargs):
        app_instance = self.controller_rest_app
        body = json.dumps(app_instance.dpids)
        return Response(content_type='application/json', body=body)

    @route('switch', '/links', methods=['GET']) 
    def get_links(self, req, **kwargs):
        app_instance = self.controller_rest_app
        body = json.dumps(app_instance.links)
        return Response(content_type='application/json', body=body)

    @route('switch', '/ports/{dpid}', methods=['GET'], requirements = { 'dpid' : dpid_lib.DPID_PATTERN }) 
    def get_ports(self, req, **kwargs):
        app_instance = self.controller_rest_app
        dpid = dpid_lib.str_to_dpid(kwargs['dpid'])
        body = json.dumps(app_instance.dpset.get_ports(dpid))
        return Response(content_type='application/json', body=body)

    @route( 'switch', '/addflows/{dpid}/{in_port}/{out_port}', methods=['POST']
          , requirements = { 'dpid' : dpid_lib.DPID_PATTERN
                           , 'in_port' : port_no_lib.PORT_NO_PATTERN
                           , 'out_port' : port_no_lib.PORT_NO_PATTERN
                           }
          )
    def add_flows(self, req, **kwargs):
        app_instance = self.controller_rest_app

        dpid     = dpid_lib.str_to_dpid(kwargs['dpid'])
        datapath = app_instance.datapaths[dpid]
        in_port  = port_no_lib.str_to_port_no(kwargs['in_port'])
        out_port = port_no_lib.str_to_port_no(kwargs['out_port'])

        app_instance.logger.info("\nHttp request add_flows \n Dpid     %s \n In Port  %s \n Out Port %s \n", dpid, in_port, out_port)
        #print "hello"

        app_instance.setup_flows(datapath, in_port, out_port)

        body = json.dumps('flow added successfully')
        return Response(content_type='application/json', body=body)
        
    @route('switch', '/create_link/{sensor}/{icontroller}', methods=['POST'],
          requirements = { 'sensor' : "[0-9]",
                           'icontroller' : "[0-9]"
                         }
           ) 
    def add_link(self, req, **kwargs):
        app_instance      = self.controller_rest_app
        sensor_index      = int(kwargs['sensor'])
        icontroller_index = int(kwargs['icontroller'])
        app_instance.logger.info("\nsensor len: %s \nsensor request index: %s \n", len(app_instance.config['sensors']), sensor_index)
        app_instance.logger.info("icontroller len: %s \nicontroller request index: %s", len(app_instance.config['icontrollers']), icontroller_index)

        if sensor_index >= len(app_instance.config['sensors']) or (
            icontroller_index >= len(app_instance.config['icontrollers'])):
            
            body = json.dumps("Sensor/Icontroller index out of range\n")
            return Response(content_type='application/json', body=body)

        app_instance.calculate_path(
                app_instance.config['sensors'][sensor_index],
                app_instance.config['icontrollers'][icontroller_index])

        app_instance.calculate_path(
                app_instance.config['icontrollers'][icontroller_index],
                app_instance.config['sensors'][sensor_index])

        body = json.dumps("link added successfully!\n")
        return Response(content_type='application/json', body=body)

    @route('switch', '/start_bfd', methods=['POST', 'GET'])
    def start_bfd(self, req, **kwargs):
        start_bfd_sessions()
        body = json.dumps("bfd sessions started successfully")
        return Response(content_type='application/json', body=body)

    @route('switch', '/primary_path/{state}', methods=['POST', 'GET'], requirements = { 'state': "[0-1]" })
    def toggle_primary_path(self, req, **kwargs):
        state = int(kwargs['state'])
        if state:
            cmd  = "sudo ovs-ofctl -O OpenFlow13 mod-port s2 1 up && sudo ovs-ofctl -O OpenFlow13 mod-port s2 2 up"
            body = json.dumps("primary path is now up")
        else:
            cmd  = "sudo ovs-ofctl -O OpenFlow13 mod-port s2 1 down && sudo ovs-ofctl -O OpenFlow13 mod-port s2 2 down"
            body = json.dumps("primary path is now down")
        
        popen(cmd, "r")
        return Response(content_type='application/json', body=body)

    @route('switch', '/backup_path/{state}', methods=['POST', 'GET'], requirements = { 'state': "[0-1]" })
    def toggle_backup_path(self, req, **kwargs):
        state = int(kwargs['state'])
        if state:
            cmd  = "sudo ovs-ofctl -O OpenFlow13 mod-port s3 1 up && sudo ovs-ofctl -O OpenFlow13 mod-port s3 2 up"
            body = json.dumps("backup path is now up")
        else:
            cmd  = "sudo ovs-ofctl -O OpenFlow13 mod-port s3 1 down && sudo ovs-ofctl -O OpenFlow13 mod-port s3 2 down"
            body = json.dumps("backup path is now down")
        
        popen(cmd, "r")
        return Response(content_type='application/json', body=body)
