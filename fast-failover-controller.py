from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, DEAD_DISPATCHER
from ryu.controller.handler import MAIN_DISPATCHER, HANDSHAKE_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.ofproto import ether
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import arp
from ryu.lib.packet import ipv4
from ryu.lib.packet import ipv6
from ryu import utils

OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]
ETHERNET = ethernet.ethernet.__name__
ARP = arp.arp.__name__

class FastFailoverController(app_manager.RyuApp):

    def __init__(self, *args, **kwargs):
        super(FastFailoverController, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.arp_table   = {}
        self.datapaths   = {}
        self.sw          = {}
        self.FLAGS       = True

    @set_ev_cls( ofp_event.EventOFPErrorMsg
               , [HANDSHAKE_DISPATCHER, CONFIG_DISPATCHER, MAIN_DISPATCHER]
               )
    def error_msg_handler(self, ev):
        msg = ev.msg
        self.logger.info('OFPErrorMsg recieved: type=0x%02x code=0x%02x '
                          'message=%s', msg.type, msg.code,
                          utils.hex_array(msg.data))

    @set_ev_cls( ofp_event.EventOFPStateChange
               , [MAIN_DISPATCHER, DEAD_DISPATCHER]
               )
    def _state_change_handler(self, ev):
        datapath = ev.datapath
        if ev.state == MAIN_DISPATCHER:
            if not datapath.id in self.datapaths:
                self.logger.info('register datapath: %016x', datapath.id)
                self.datapaths[datapath.id] = datapath
            elif ev.state == DEAD_DISPATHCER:
                if datapath.id in self.datapaths:
                    self.logger.info('unregister datapath: %016x', datapath.id)
                    del self.datapaths[datapath.id]

    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath    = ev.msg.datapath
        dpid        = datapath.id
        ofp         = datapath.ofproto
        ofp_parser  = datapath.ofproto_parser

        match   = ofp_parser.OFPMatch()
        actions = [ofp_parser.OFPActionOutput(ofp.OFPP_CONTROLLER, ofp.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, 0, match, actions)
        self.logger.info("Switch: %s Connected", dpid)

    def add_flow(self, datapath, hard_timeout, priority, match, actions):
        ofp         = datapath.ofproto
        ofp_parser  = datapath.ofproto_parser

        inst = [ofp_parser.OFPInstructionActions(ofp.OFPIT_APPLY_ACTIONS, actions)]
        mod = ofp_parser.OFPFlowMod( datapath       = datapath
                                   , priority       = priority
                                   , hard_timeout   = hard_timeout
                                   , match          = match
                                   , instructions   = inst
                                   )
        datapath.send_msg(mod)

    def _build_packet_out( self
                         , datapath
                         , buffer_id
                         , src_port
                         , dst_port
                         , data
                         ):
        actions = []
        if dst_port:
            actions.append(datapath.ofproto_parser.OFPActionOutput(dst_port))

        msg_data = None
        if buffer_id == datapath.ofproto.OFP_NO_BUFFER:
            if data is None:
                return None
            msg_data = data

        out = datapath.ofproto_parser.OFPPacketOut( datapath    = datapath
                                                  , buffer_id   = buffer_id
                                                  , data        = msg_data
                                                  , in_port     = src_port
                                                  , actions     = actions
                                                  )
        return out

    def send_packet_out( self
                       , datapath
                       , buffer_id
                       , src_port
                       , dst_port
                       , data
                       ):
        out = self._build_packet_out( datapath
                                    , buffer_id
                                    , src_port
                                    , dst_port
                                    , data
                                    )

        if out:
            datapath.send_msg(out)

    def flood(self, msg):
        datapath    = msg.datapath
        ofp         = datapath.ofproto
        ofp_parser  = datapath.ofproto_parser
        out = self._build_packet_out( datapath
                                    , ofp.OFP_NO_BUFFER
                                    , ofp.OFPP_CONTROLLER
                                    , ofp.OFPP_FLOOD
                                    , msg.data
                                    )
    '''
    def arp_forwarding( self
                      , msg
                      , src_ip
                      , dst_ip
                      , eth_pkt
                      ):
        datapath    = msg.datapath
        dpid        = datapath.id
        ofp_parser  = datapath.ofproto_parser
        dst         = eth_pkt.dst

        in_port     = msg.match['in_port']
        out_port    = self.mac_to_port[dpid].get(dst)
        
        if out_port is not None:
            match = ofp_parser.OFPMatch( in_port = in_port
                                       , eth_dst = dst
                                       , eth_type = eth_pkt.ethertype
                                       )
            actions = [ofp_parser.OFPActionOutput(out_port)]
            self.add_flow(datapath, 0, 1, match, actions)
            self.send_packet_out( datapath
                                , msg.buffer_id
                                , in_port
                                , out_port
                                , msg.data
                                )
            self.logger.info("Reply ARP to knew host")
        else:
            self.flood(msg)
    '''

    def arp_handler(self, header_list, datapath, in_port, msg_buffer_id):
        self.logger.info("Entered arp handler. \n header_list: %s", header_list)
        header_list = header_list
        datapath = datapath
        in_port = in_port
        dpid = datapath.id
 
        """
        If header_list contains ETHERNET, get source and destination MAC
        """
        if ETHERNET in header_list:
            eth_dst = header_list[ETHERNET].dst
            eth_src = header_list[ETHERNET].src
 
        """
        Part of the Loop Prevention
 
        If the packet is ARP and it is broadcast, then get dst_ip. And if local 
        has key (dpid, eth_src, dst_ip), if the value of key is not in_port, then 
        send the packet. Otherwise, add the key into dictionary, and set its 
        value to in_port
 
        In this case, the very first ARP broadcast packet will be recorded 
        according to the in_port, and the rest of the coming ARP packets will
        be checked with key (dpid, src, dst_ip):
           1. If coming packet's key is existed, and from another in_port (flood)
           2. If coming packet's key is existed, and from the original in_port 
           (drop)
           3. If coming packet's key is new (which is a very first ARP packet)
        """
        _arp = 'arp' in header_list
        self.logger.info("eth_dst: %s \n ARP in header_list?:", eth_dst, True.string)
        print ("ARP in header_list" _arp)
        if eth_dst == 'ff:ff:ff:ff:ff:ff' and ARP in header_list:
            arp_dst_ip = header_list[ARP].dst_ip
            if (dpid, eth_src, arp_dst_ip) in self.sw:
                if self.sw[(datapath.id, eth_src, arp_dst_ip)] != in_port:
                    out = datapath.ofproto_parser.OFPPacketOut(
                        datapath=datapath,
                        buffer_id=datapath.ofproto.OFP_NO_BUFFER,
                        in_port=in_port,
                        actions=[], 
                        data=None)
                    datapath.send_msg(out)
                    return True
            else:
                self.sw[(dpid, eth_src, arp_dst_ip)] = in_port
 
        """
        Part of the ARP Proxy
 
        If the packet is ARP and it is not broadcast, get information in detail 
        such as operation code (REQUEST/REPLY)
        """
        if ARP in header_list:
            hwtype = header_list[ARP].hwtype
            proto = header_list[ARP].proto
            hlen = header_list[ARP].hlen
            plen = header_list[ARP].plen
            opcode = header_list[ARP].opcode
            arp_src_ip = header_list[ARP].src_ip
            arp_dst_ip = header_list[ARP].dst_ip
            actions = []
 
            """
            packet is an ARP request, if it is learnt already, then reply it
            """
            if opcode == arp.ARP_REQUEST:
                if arp_dst_ip in self.arp_table:
                    actions.append(datapath.ofproto_parser.OFPActionOutput(
                    in_port))
 
                    ARP_Reply = packet.Packet()
                    ARP_Reply.add_protocol(ethernet.ethernet(
                        ethertype=header_list[ETHERNET].ethertype,
                        dst=eth_src,
                        src=self.arp_table[arp_dst_ip]))
                    ARP_Reply.add_protocol(arp.arp(
                        opcode=arp.ARP_REPLY,
                        src_mac=self.arp_table[arp_dst_ip],
                        src_ip=arp_dst_ip,
                        dst_mac=eth_src,
                        dst_ip=arp_src_ip))
 
                    ARP_Reply.serialize()
 
                    out = datapath.ofproto_parser.OFPPacketOut(
                        datapath=datapath,
                        buffer_id=datapath.ofproto.OFP_NO_BUFFER,
                        in_port=datapath.ofproto.OFPP_CONTROLLER,
                        actions=actions, 
                        data=ARP_Reply.data)
                    datapath.send_msg(out)
                    return True
        return False

    def mac_learning(self, dpid, src_mac, in_port):
        self.mac_to_port.setdefault(dpid, {})

        if src_mac in self.mac_to_port[dpid]:
            if in_port != self.mac_to_port[dpid][src_mac]:
                return False
            else:
                return True
        else:
            self.mac_to_port[dpid][src_mac] = in_port
            return True

    def send_group_mod(self, datapath):
        ofp = datapath.ofproto
        ofp_parser = datapath.ofproto_parser

        group_id = 50
        watch_port_1, watch_port_2 = 2, 3
        port_2, port_3 = watch_port_1, watch_port_2
        watch_group = OFPG_ANY
        actions_1 = [ofp_parser.OFPActionOutput(port_2)]
        actions_2 = [ofp_parser.OFPActionOutput(port_3)]

        bucket_list = [ ofp_parser.OFPBucket(0, watch_port_1, watch_group, actions_1)
                      , ofp_parser.OFPBucket(0, watch_port_2, watch_group, actions_2)]

        req = ofp_parser.OFPGroupMod( datapath
                                    , ofp.OFPFC_ADD
                                    , ofp.OFPGT_FF
                                    , group_id
                                    , bucket_list
                                    )

        datapath.send_msg(req)

    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        msg         = ev.msg
        datapath    = msg.datapath
        dpid        = datapath.id
        ofp_parser  = datapath.ofproto_parser
        in_port     = msg.match['in_port']

        self.logger.info("Packet from Switch: %s arrived", dpid)
        self.logger.info("datapath : %s", datapath)
        self.logger.info("In port  : %s", in_port)

        pkt         = packet.Packet(msg.data)
        eth_pkt     = pkt.get_protocols(ethernet.ethernet)[0]
        eth_src     = eth_pkt.src
        arp_pkt     = pkt.get_protocol(arp.arp)
        ip_pkt      = pkt.get_protocol(ipv4.ipv4)
        ip_pkt_6    = pkt.get_protocol(ipv6.ipv6)

        header_list = dict((p.protocol_name, p) for p in pkt)

        if ARP in header_list:
            self.arp_table[header_list[ARP].src_ip] = eth_src
            self.logger.info("ARP src IP: %s", header_list[ARP].src_ip)

        if isinstance(ip_pkt_6, ipv6.ipv6):
            self.logger.info("IPv6 Processing")
            actions = []
            match = ofp_parser.OFPMatch(eth_type=ether.ETH_TYPE_IPV6)
            self.add_flow(datapath, 0, 1, match, actions)
            return

        if isinstance(arp_pkt, arp.arp):
            self.logger.info("ARP Processing")
            if self.mac_learning(dpid, eth_pkt.src, in_port) is False:
                self.logger.info("ARP packet enters in another port")
                return
            '''self.arp_forwarding(msg, arp_pkt.src_ip, arp_pkt.dst_ip, eth_pkt) '''
            self.arp_handler(header_list, datapath, arp_pkt.dst_ip, eth_pkt) 

        if isinstance(ip_pkt, ipv4.ipv4):
            self.logger.info("IPV4 processing")
            out_port = None
            if eth_pkt.dst in self.mac_to_port[dpid]:
                if dpid == 1 and in_port == 1:
                    if self.FLAGS is True:
                        self.send_group_mod(datapath)
                        self.logger.info("Group injected")
                        self.FLAGS = False

                    actions = [ofp_parser.OFPActionGroup(group_id=50)]
                    match = ofp_parser.OFPMatch( in_port = in_port
                                               , eth_type = eth_pkt.ethertype
                                               , ipv4_src = ip_pkt.src
                                               )
                    self.add_flow(datapath, 0, 3, match, actions)
                    self.send_packet_out( datapath
                                        , msg.buffer_id
                                        , in_port
                                        , out_port
                                        , msg.data
                                        )
                else:
                    out_port = self.mac_to_port[dpid][eth_pkt.dst]
                    actions  = [ofp_parser.OFPActionOutput(out_port)]
                    match = ofp_parser.OFPMatch( in_port = in_port
                                               , eth_dst = eth_pkt.dst
                                               , eth_type = eth_pkt.ethertype)
                    self.add_flow(datapath, 0, 1, match, actions)
                    self.send_packet_out( datapath
                                        , msg.buffer_id
                                        , in_port
                                        , out_port
                                        , msg.data
                                        )
            else:
                if self.mac_learning(dpid,  eth_pkt.src, in_port) is False:
                    self.logger.info("IPv4 packet enter in different ports")
                    return
                else:
                    self.flood(msg)
