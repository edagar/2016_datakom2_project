# Fast-Failover-Demo Topology
from mininet.topo import Topo 
from mininet.cli import CLI 
from mininet.net import Mininet 
from mininet.link import TCLink 
from mininet.util import irange,dumpNodeConnections 
from mininet.log import setLogLevel 
from mininet.node import Controller, RemoteController, Ryu

class FFTopo(Topo): 
    """Topology for fast failover demo of OpenFlow 1.3 groups."""
 
    def __init__(self): 
        # Initialize topology and default options 
        Topo.__init__(self) 

        s1 = self.addSwitch('s1',dpid='0000000000000001', protocols="OpenFlow13")
        s3 = self.addSwitch('s3',dpid='0000000000000003', protocols="OpenFlow13")
        s2 = self.addSwitch('s2',dpid='0000000000000002', protocols="OpenFlow13")
        s4 = self.addSwitch('s4',dpid='0000000000000004', protocols="OpenFlow13")

        self.addLink(s1, s3) 
        self.addLink(s1, s2) 
        self.addLink(s4, s3) 
        self.addLink(s4, s2) 

        host_1 = self.addHost('h1',ip='10.0.0.1',mac='10:00:00:00:00:01') 
        host_2 = self.addHost('h2',ip='10.0.0.2',mac='10:00:00:00:00:02')        

        self.addLink(host_1, s1) 
        self.addLink(host_2, s4)   

topos = { 'ff_topo': (lambda: FFTopo()) }
