FILE = "ffcontroller.conf"

def load():
    """ 
    parse config file and return a dict
    with the corresponding values
    """
    def parse_link(line):
        if len(line) < 6:
            return None

        link = {}
        values = line[5:-1].replace(" ", "").split(",")

        if len(values) < 3:
            return None

        link["dp"] = values[0]
        link["port"] = values[1]
        link["mac"] = values[2]

        return link

    def parse_kw(line):
        l = line.split(" ")
        if len(l) < 2:
            return None
        return l[1]

    conf = {
            "links": [],
            "sensors": [],
            "icontrollers": []
            }

    with open(FILE, "r") as f:
        for line in f.readlines():
            if line.startswith("#") or line == "\n":
                continue

            elif line.startswith("link("):
                conf['links'].append(parse_link(line.strip()))

            elif line.startswith("sensor "):
                conf['sensors'].append(parse_kw(line.strip()))

            elif line.startswith("icontroller "):
                conf['icontrollers'].append(parse_kw(line.strip()))

                
    return conf

print load()
