import Html exposing (..)
import Html.App as Html
import Html.Attributes exposing (..)
import Html.Events exposing (..)
import Http
import Json.Decode exposing (..)
import Task

ryu_url ="http://192.168.122.1:8081/"
--"http://127.0.0.1:8080/start_bfd"
            

main =
  Html.program
    { init = init "0000000000000001"
    , view = view
    , update = update
    , subscriptions = subscriptions
    }



-- MODEL

type alias Model =
    { dpids : List Int
    , ports : List Int
    , input_addflows_switch : String
    , input_addflows_in_port : String
    , input_addflows_out_port : String
    , status_addflows : String
    , input_create_link_sensor : String
    , input_create_link_icontroller : String
    , status_create_link : String
    , status_start_bfd : String
    , input_list_connected_switch : String
    }

init : String -> (Model, Cmd Msg)
init mac =
    ( Model [] [] "" "" "" "status"  "" "" "status" "status"  ""
    , Cmd.none
    )


-- UPDATE


type Msg
  = Addflows
  | InputAddflowsSwitch String
  | InputAddflowsInPort String
  | InputAddflowsOutPort String
  | FailAddflows Http.Error
  | SuccedAddflows String
  | CreateLink
  | InputCreateLinkSensor String
  | InputCreateLinkIController String
  | SuccedCreateLink String
  | FailCreateLink Http.Error
  | StartBfd
  | SuccedStartBfd String
  | FailStartBfd Http.Error
{-- | ListConnectedPorts
  | ListConnectedPortsSwitch String --}
--  | SuccedListConnectedPorts String


update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
    case msg of
        Addflows ->
            (model, addFlows model.input_addflows_switch
                model.input_addflows_in_port
                model.input_addflows_out_port)

        InputAddflowsSwitch switch ->
            ({ model | input_addflows_switch = switch }, Cmd.none)

        InputAddflowsInPort in_port ->
            ({ model | input_addflows_in_port = in_port }, Cmd.none)

        InputAddflowsOutPort out_port ->
            ({ model | input_addflows_out_port = out_port }, Cmd.none)

        SuccedAddflows status ->
            ({ model | status_addflows = status}, Cmd.none)

        FailAddflows httpError ->
            ({ model | status_addflows = getError httpError}, Cmd.none)


        CreateLink ->
            (model, createLink model.input_create_link_sensor
                model.input_create_link_icontroller)

        InputCreateLinkSensor sensor ->
            ({ model | input_create_link_sensor = sensor }, Cmd.none)

        InputCreateLinkIController icontroller ->
            ({ model | input_create_link_icontroller = icontroller }, Cmd.none)

        SuccedCreateLink status ->
            ({ model | status_create_link = status}, Cmd.none)

        FailCreateLink httpError ->
            ({ model | status_create_link = getError httpError}, Cmd.none)


        StartBfd ->
            (model, startBfd)

        SuccedStartBfd status ->
            ({ model | status_start_bfd = status }, Cmd.none)

        FailStartBfd httpError ->
            ({ model | status_start_bfd = getError httpError}, Cmd.none)



getError : Http.Error -> String
getError error =
    case error of
        Http.Timeout ->
            "Timeout"
        Http.NetworkError ->
            toString error
        Http.UnexpectedPayload payload ->
            payload
        Http.BadResponse status response ->
            "Status: " ++ (toString status)
        

-- VIEW


view : Model -> Html Msg
view model =
  div []
    [ h2 [] [text "OpenFlow Controller Interface"]
    , br [] []
    , div []
        [   button [ onClick Addflows] [text "Add Flows"]
        ,   input [ placeholder "Datapath id", onInput InputAddflowsSwitch] []
        ,   input [ placeholder "In Port",     onInput InputAddflowsInPort] []
        ,   input [ placeholder "Out Port",    onInput InputAddflowsOutPort] []
        , text model.status_addflows
        ]
    , br [] []
    , div []
        [   button [ onClick CreateLink] [text "Create Link"]
        ,   input [ placeholder "Sensor", onInput InputCreateLinkSensor] []
        ,   input [ placeholder "Industrial Controller", onInput InputCreateLinkIController] []
        , text model.status_create_link
        ]
    , br [] []
    , div []
        [ button [ onClick StartBfd] [text "Start Bfd"]
        , text model.status_start_bfd
        ]
    ]


-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
  Sub.none


-- HTTP

addFlows: String -> String -> String -> Cmd Msg
addFlows switch in_port out_port =
    let
        url =
            ryu_url ++ "addflows/" ++
                switch ++ "/" ++
                in_port ++ "/" ++
                out_port
    in
        Task.perform FailAddflows SuccedAddflows (Http.post decoderAddflows url
            (Http.string ""))

decoderAddflows: Decoder String
decoderAddflows =
   string 

            
createLink: String -> String -> Cmd Msg
createLink sensor icontroller =
    let
        url =
            ryu_url ++ "create_link/" ++
                sensor ++ "/" ++
                icontroller
    in
        Task.perform FailCreateLink SuccedCreateLink (Http.post decoderCreateLink url
            (Http.string ""))

decoderCreateLink: Decoder String
decoderCreateLink =
   string 


startBfd: Cmd Msg
startBfd =
    let
        url =
            ryu_url ++ "start_bfd"
    in
        Task.perform FailStartBfd SuccedStartBfd (Http.get decoderStartBfd url)

decoderStartBfd: Decoder String
decoderStartBfd =
   string 

