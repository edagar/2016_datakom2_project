#!/usr/bin/python

from os import popen
from functools import partial
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.log import setLogLevel, info
from mininet.node import Controller, RemoteController, Ryu

def start_bfd_sessions():
    def enable_bfd(iface):
        popen('sudo ovs-vsctl set interface %s bfd:enable=true' % iface, 'r')
        popen('sudo ovs-appctl bfd/set-forwarding %s true' % iface, 'r')

    lines = popen('sudo ovs-vsctl list interface | grep "name " | grep "s[0-9]-eth[0-9]"', 'r').readlines()
    lines =  [l.split(":")[1].strip().replace('"', "") for l in lines]
    [enable_bfd(iface) for iface in lines]

def init_topo(net):
    c0 = net.addController(
            'c0',
            controller=RemoteController,
            ip='127.0.0.1',
            port=6633)

    """
    build a topology based on:
    https://floodlight.atlassian.net/wiki/display/floodlightcontroller/How+to+Work+with+Fast-Failover+OpenFlow+Groups#HowtoWorkwithFast-FailoverOpenFlowGroups-ExperimentDesign
    """
    h = [net.addHost('h%d' % (x+1)) for x in range(2)]
    s = [net.addSwitch('s%d' % (x+1), protocols='OpenFlow13') for x in range(4)]

    net.addLink(h[0], s[0])

    net.addLink(s[0], s[1])
    net.addLink(s[0], s[2])

    net.addLink(s[1], s[3])

    net.addLink(s[2], s[3])

    net.addLink(s[3], h[1])

    net.build()

    c0.start()
    [x.start([c0]) for x in s]

def run():
    setLogLevel('info')
    net = Mininet(topo=None, link=TCLink, build=False)

    init_topo(net)

    # start bfd sessions between the switches
    start_bfd_sessions()

    # start the mininet REPL
    CLI(net)

    net.stop()
    
if __name__ == '__main__':
    run()
