"""
Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""
 
from mininet.topo import Topo
 
class MyTopo(Topo):
    def __init__(self):
        Topo.__init__(self)
 
        host1 = self.addHost('h1')
        host2 = self.addHost('h2')
        host3 = self.addHost('h3')
        #host4 = self.addHost('h4')
        switch1 = self.addSwitch("s1")
        switch2 = self.addSwitch("s2")
        switch3 = self.addSwitch("s3")
        switch4 = self.addSwitch("s4")
 
        self.addLink(switch1, host1, 1)
        self.addLink(switch1, switch2, 2, 1)
        self.addLink(switch1, switch3, 3, 1)
        self.addLink(switch2, switch4, 2, 1)
        self.addLink(switch3, switch4, 2, 2)
        self.addLink(switch4, host2, 3)
        self.addLink(switch4, host3, 4)
        #self.addLink(switch3, host4, 3)
 
topos = {'mytopo': (lambda: MyTopo())}
